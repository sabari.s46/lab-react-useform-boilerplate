import React from "react";
import { useForm } from "react-hook-form";
import "./App.css";

const App = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    // Handle form submission logic here
    console.log(data);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div id="items">
          <input
            type="text"
            {...register("first_name", {
              required: "Please enter your first name",
            })}
            placeholder="First Name"
          />
          <p className="error">{errors.first_name?.message}</p>

          <input
            type="text"
            {...register("last_name", {
              required: "Please enter your last name",
            })}
            placeholder="Last Name"
          />
          <p className="error">{errors.last_name?.message}</p>
          <input
            type="text"
            {...register("email", {
              required: "Please enter your email",
              pattern: {
                value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
                message: "Please enter a valid email",
              },
            })}
            placeholder="Email"
          />
          <p className="error">{errors.email?.message}</p>

          <input
            type="number"
            {...register("contact", {
              required: "Please enter your contact",
              minLength: {
                value: 10,
                message: "Please enter a valid contact",
              },
            })}
            placeholder="Contact"
          />
          <p className="error">{errors.contact?.message}</p>

          <button type="submit">Register</button>
        </div>
      </form>
    </>
  );
};

export default App;
